<?php include "inc/header.php"?>
<?php include "inc/slider.php"?>


	<div class="contentsection contemplete clear">
		<div class="maincontent clear">
            <!--Pagination-->
                <?php
                $per_page = 3;
                if (isset($_GET["page"])){
                    $page = $_GET["page"];
                }
                else{
                    $page = 1;
                }
                $start_form = ($page - 1) * $per_page;
                ?>
            <!--Pagination-->


            <?php
            $query = "select * from tbl_post limit $start_form, $per_page";
            $post = $db->select($query);
            if ($post) {
            while ($result = $post->fetch_assoc()) {
            ?>

			<div class="samepost clear">
                <h2><a href="post.php?id=<?php echo $result['id'];?>"><?php echo $result['title'];?></a></h2>
                <h4><?php echo $fm->formatDate($result['date']);?>, By <a href="#"><?php echo $result['author'];?></a></h4>
				 <a href="#"><img src="admin/<?php echo $result['image'];?>" alt="post image"/></a>

                <?php echo $fm->textShorten($result['body']);?>

                <div class="readmore clear">
                    <h2><a href="post.php?id=<?php echo $result['id'];?>">Read More</a></h2>
                </div>
			</div>

            <?php } ?><!--End While Loop-->

                <!--Pagination-->
                <?php
                $query = "select * from tbl_post";
                $result = $db->select($query);
                $total_rows = mysqli_num_rows($result);
                $total_pages = ceil($total_rows / $per_page);



                echo "<span style='display: block; font-size: 20px; margin-top: 10px; text-align: center; padding: 10px;'>
                    <a style='border: 1px solid; text-decoration: none; padding: 5px; background-color: sandybrown' href='index.php?page=1'>".'First Page'."</a>";
                for ($i = 1; $i <= $total_pages; $i++){
                    echo "<a style='border: 1px solid; text-decoration: none; padding: 5px; background-color: sandybrown' href='index.php?page=".$i."'>".$i."</a>";
                }

                echo "<a style='border: 1px solid; text-decoration: none; padding: 5px; background-color: sandybrown' href='index.php?page=$total_pages'>".'Last Page'."</a></span>"?>

            <?php } else{ header("Location:404.php"); }?>
		</div>

<?php include "inc/sidebar.php"?>
<?php include "inc/footer.php"?>
