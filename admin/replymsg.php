<?php include 'inc/header.php' ?>
<?php include 'inc/sidebar.php' ?>
<?php
if (!isset($_GET['replyid']) || $_GET['replyid'] == NULL){
    header("Location:inbox.php");
}else{
    $id = $_GET['replyid'];
}
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>View Message</h2>
        <?php
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $to = $fm->validation($_POST['toEmail']);
            $from = $fm->validation($_POST['fromEmail']);
            $subject = $fm->validation($_POST['subject']);
            $message = $fm->validation($_POST['message']);
            $sendMessage = mail($to, $subject, $message, $from);
            if ($sendMessage){
                echo "<span style='color: red; font-size: 18px;'>Message Sent Successfully!</span>";
            } else{
            echo "<span style='color: red; font-size: 18px;'>Message Not Sent!</span>";
            }

        }
        ?>

        <div class="block">
            <form action="" method="post">

                <?php
                $query = "SELECT * FROM tbl_contact WHERE id = '$id'";
                $msg = $db->select($query);
                if ($msg){
                    while ($result = $msg->fetch_assoc()){
                        ?>

                        <table class="form">

                            <tr>
                                <td>
                                    <label>To</label>
                                </td>
                                <td>
                                    <input type="text" readonly name="toEmail" value="<?php echo $result['email']; ?>" class="medium" />
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <label>From</label>
                                </td>
                                <td>
                                    <input type="text" name="fromEmail" placeholder="Please Enter Your Email Addresss" class="medium" />
                                </td>
                            </tr>



                            <tr>
                                <td>
                                    <label>Subject</label>
                                </td>
                                <td>
                                    <input type="text" name="subject" placeholder="Please Enter Your Subject" class="medium" />
                                </td>
                            </tr>



                            <tr>
                                <td style="vertical-align: top; padding-top: 9px;">
                                    <label>Message</label>
                                </td>
                                <td>
                                    <textarea class="tinymce" name="message">

                                    </textarea>
                                </td>
                            </tr>



                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="submit" Value="Send" />
                                </td>
                            </tr>
                        </table>
                    <?php }} ?>
            </form>
        </div>
    </div>
</div>
<div class="clear">
</div>

<?php include 'inc/footer.php' ?>
