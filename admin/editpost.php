<?php include 'inc/header.php' ?>
<?php include 'inc/sidebar.php' ?>
<?php
if (!isset($_GET['editid']) || $_GET['editid'] == NULL){
    header("Location:postlist.php");
}else{
    $id = $_GET['editid'];
}
?>
<div class="grid_10">

    <div class="box round first grid">
        <h2>Update Post</h2>
        <?php
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // $name = $_POST['name'];
            $title = mysqli_real_escape_string($db->link, $_POST['title']);
            $cat = mysqli_real_escape_string($db->link, $_POST['cat']);
            $body = mysqli_real_escape_string($db->link, $_POST['body']);
            $tags = mysqli_real_escape_string($db->link, $_POST['tags']);
            $author = mysqli_real_escape_string($db->link, $_POST['author']);

            $permited = array('jpg', 'jpeg', 'png', 'gif');
            $file_name = $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $file_temp = $_FILES['image']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
            $uploaded_image = "upload/" . $unique_image;

            if ($title == "" || $cat == "" || $body == "" || $tags == "" || $author == "") {
                echo "<span style='font-size: 18px; color: red;'>Field Must Not Be Empty!</span>";
            }
            else {
                if (!empty($file_name)) {
                    if ($file_size > 1048567) {
                        echo "<span style='font-size: 18px; color: red;'>Image Size should be less then 1MB!
     </span>";
                    } elseif (in_array($file_ext, $permited) === false) {
                        echo "<span style='font-size: 18px; color: red;'>You can upload only:-"
                            . implode(', ', $permited) . "</span>";
                    } else {
                        move_uploaded_file($file_temp, $uploaded_image);
                        $query = "UPDATE tbl_post
                              SET
                              cat = '$cat',
                              title = '$title',
                              body = '$body',
                              image = '$uploaded_image',
                              author = '$author',
                              tags = '$tags'
                              WHERE id = '$id'
                              ";
                        $updated_row = $db->update($query);
                        if ($updated_row) {
                            echo "<span style='font-size: 18px; color: green;'>Post Updated Successfully.</span>";
                        } else {
                            echo "<span style='font-size: 18px; color: red;'>Post Not Updated !</span>";
                        }
                    }

                } else {
                    $query = "UPDATE tbl_post
                              SET
                              cat = '$cat',
                              title = '$title',
                              body = '$body',
                              author = '$author',
                              tags = '$tags'
                              WHERE id = '$id'
                              ";
                    $updated_row = $db->update($query);
                    if ($updated_row) {
                        echo "<span style='font-size: 18px; color: green;'>Post Updated Successfully.</span>";
                    } else {
                        echo "<span style='font-size: 18px; color: red;'>Post Not Updated !</span>";
                    }
                }
            }

        }
        ?>

        <div class="block">
            <?php
            $query = "SELECT * FROM tbl_post where id='$id' order by id desc";
            $post = $db->select($query);
            while ($post_result = $post->fetch_assoc()){
            ?>

            <form action="" method="post" enctype="multipart/form-data">
                <table class="form">

                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <input type="text" name="title" value="<?php echo $post_result['title'];?>" class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Category</label>
                        </td>
                        <td>
                            <select id="select" name="cat">
                                <option>Select Category</option>

                                <?php
                                $query = "SELECT * FROM tbl_category";
                                $catquery = $db->select($query);
                                if ($catquery){
                                    while ($result = $catquery->fetch_assoc()){

                                        ?>
                                        <option
                                            <?php if ($post_result['cat'] == $result['id']){
                                                echo "selected"; ?>
                                            <?php } ?>
                                                value="<?php echo $result['id'];?>"><?php echo $result['name'];?></option>

                                    <?php } } ?>

                            </select>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <img src="<?php echo $post_result['image'];?>" width="60px" height="40px"><br>
                            <input type="file" name="image"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Content</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body"><?php echo $post_result['body'];?></textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Tags</label>
                        </td>
                        <td>
                            <input type="text" name="tags" value="<?php echo $post_result['tags'];?>" class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Author</label>
                        </td>
                        <td>
                            <input type="text" name="author" value="<?php echo $post_result['author'];?>" class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
            <?php } ?>
        </div>
    </div>
</div>
<div class="clear">
</div>

<?php include 'inc/footer.php' ?>
