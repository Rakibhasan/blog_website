<?php
include "../lib/Session.php";
Session::checkSession();
?>

<?php include "../config/config.php"?>
<?php include "../lib/Database.php"?>
<?php include "../helpers/Format.php"?>

<?php
$db = new Database();
?>
<?php
if (!isset($_GET['deleteid']) || $_GET['deleteid'] == NULL){
    header("Location:postlist.php");
}else{
    $id = $_GET['deleteid'];

    $query = "select * from tbl_post where id = '$id'";
    $result = $db->select($query);
    if ($result){
        while ($delimg = $result->fetch_assoc()){
            $dellink = $delimg['image'];
            unlink($dellink);
        }
    }
    $delquery = "delete from tbl_post where id = '$id'";
    $delresult = $db->delete($delquery);
    if ($delresult){
        echo "<script>alert('Data Deleted Successfully!');</script>";
        echo "<script>window.location = 'postlist.php';</script>";
    }else{
        echo "<script>alert('Data Not Deleted Successfully!');</script>";
        header("Location:postlist.php");
    }
}
?>
