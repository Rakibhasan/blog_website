﻿<?php include 'inc/header.php' ?>
<?php include 'inc/sidebar.php'?>


        <div class="grid_10">
            <div class="box round first grid">
                <h2>Inbox</h2>
                <?php
                if (isset($_GET['seenid'])){
                    $seenid = $_GET['seenid'];
                    $query = "UPDATE tbl_contact SET status = '1' WHERE id = '$seenid'";
                    $upadated_row = $db->update($query);
                    if ($upadated_row){
                        echo "<span style='font-size: 18px; color: green;'>Message Sent In The Seen Box!</span>";
                    }else{
                        echo "<span style='font-size: 18px; color: red;'>Message Not Sent In The Seen Box!</span>";
                    }
                }

                ?>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Message</th>
							<th>Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

                    <?php
                    $query = "SELECT * FROM tbl_contact WHERE status='0' ORDER BY id DESC";
                    $msg = $db->select($query);
                    if ($msg){
                    $i = 0;
                    while ($result = $msg->fetch_assoc()){
                    $i++;
                    ?>
						<tr class="odd gradeX">
							<td><?php echo $i; ?></td>
                            <td><?php echo $result['firstname'].' '.$result['lastname']; ?></td>
                            <td><?php echo $result['email']; ?></td>
                            <td><?php echo $fm->textShorten($result['body'], '40'); ?></td>
                            <td><?php echo $fm->formatDate($result['date']); ?></td>
							<td>
                                <a href="viewmsg.php?viewid=<?php echo $result['id']; ?>">View</a> ||
                                <a href="replymsg.php?replyid=<?php echo $result['id']; ?>">Reply</a> ||
                                <a onclick="return confirm('Are You Sure To Move The Seen Box!')" href="?seenid=<?php echo $result['id']; ?>">Seen</a> ||
                            </td>
						</tr>
                    <?php }} ?>
					</tbody>
				</table>
               </div>
            </div>
            <div class="box round first grid">
                <h2>Seen Message</h2>
                <?php
                if (isset($_GET['dltid'])){
                    $dltid = $_GET['dltid'];
                    $delquery = "delete from tbl_contact where id = '$dltid'";
                    $delresult = $db->delete($delquery);
                    if ($delresult){
                        echo "<span style='font-size: 18px; color: green;'>Message Deleted Successfully!</span>";
                    }else{
                        echo "<span style='font-size: 18px; color: red;'>Message Not Deleted!</span>";
                    }

                }
                ?>
                <div class="block">
                    <table class="data display datatable" id="example">
                        <thead>
                        <tr>
                            <th>Serial No.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $query = "SELECT * FROM tbl_contact WHERE status='1' ORDER BY id DESC";
                        $msg = $db->select($query);
                        if ($msg){
                            $i = 0;
                            while ($result = $msg->fetch_assoc()){
                                $i++;
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $result['firstname'].' '.$result['lastname']; ?></td>
                                    <td><?php echo $result['email']; ?></td>
                                    <td><?php echo $fm->textShorten($result['body']); ?></td>
                                    <td><?php echo $fm->formatDate($result['date']); ?></td>
                                    <td>
                                        <a href="viewmsg.php?viewid=<?php echo $result['id']; ?>">View</a> ||
                                        <a onclick="return confirm('Are You Sure To Delete!')" href="?dltid=<?php echo $result['id']; ?>">Delete</a> ||
                                    </td>
                                </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
               </div>
            </div>
        </div>
        <div class="clear">
        </div>

<?php include 'inc/footer.php' ?>
