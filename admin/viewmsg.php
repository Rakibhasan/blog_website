<?php include 'inc/header.php' ?>
<?php include 'inc/sidebar.php' ?>
<?php
if (!isset($_GET['viewid']) || $_GET['viewid'] == NULL){
    header("Location:inbox.php");
}else{
    $id = $_GET['viewid'];
}
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>View Message</h2>
        <?php
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            echo "<script>window.location = 'inbox.php';</script>";
        }
        ?>

        <div class="block">
            <form action="" method="post">

                <?php
                $query = "SELECT * FROM tbl_contact WHERE id = '$id'";
                $msg = $db->select($query);
                if ($msg){
                while ($result = $msg->fetch_assoc()){
                ?>

                <table class="form">

                    <tr>
                        <td>
                            <label>Name</label>
                        </td>
                        <td>
                            <input type="text" readonly value="<?php echo $result['firstname'].' '.$result['lastname']; ?>" class="medium" />
                        </td>
                    </tr>


                    <tr>
                        <td>
                            <label>Email</label>
                        </td>
                        <td>
                            <input type="text" readonly value="<?php echo $result['email']; ?>" class="medium" />
                        </td>
                    </tr>



                    <tr>
                        <td>
                            <label>Date</label>
                        </td>
                        <td>
                            <input type="text" readonly value="<?php echo $result['date']; ?>" class="medium" />
                        </td>
                    </tr>



                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Message</label>
                        </td>
                        <td>
                            <textarea class="tinymce" readonly><?php echo $result['body']; ?></textarea>
                        </td>
                    </tr>



                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Ok" />
                        </td>
                    </tr>
                </table>
                <?php }} ?>
            </form>
        </div>
    </div>
</div>
<div class="clear">
</div>

<?php include 'inc/footer.php' ?>
