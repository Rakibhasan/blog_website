<?php
include "../lib/Session.php";
Session::checkSession();
?>

<?php include "../config/config.php"?>
<?php include "../lib/Database.php"?>

<?php
$db = new Database();
?>
<?php
if (!isset($_GET['delpage']) || $_GET['delpage'] == NULL){
    header("Location:index.php");
}else{
    $id = $_GET['delpage'];


    $delquery = "delete from tbl_page where id = '$id'";
    $delresult = $db->delete($delquery);
    if ($delresult){
        echo "<script>alert('Page Deleted Successfully!');</script>";
        echo "<script>window.location = 'index.php';</script>";
    }else{
        echo "<script>alert('Page Not Deleted Successfully!');</script>";
        header("Location:index.php");
    }
}
?>
