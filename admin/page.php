<?php include 'inc/header.php' ?>
<?php include 'inc/sidebar.php' ?>
<?php
if (!isset($_GET['pageid']) || $_GET['pageid'] == NULL){
    header("Location:index.php");
}else{
    $id = $_GET['pageid'];
}
?>

<style>
    .delete{
        font-size: 16px;
        border: 1px solid #d1d1d1;
        padding: 5px;
        background-color: #ddddff;
    }
</style>

<div class="grid_10">

    <div class="box round first grid">
        <h2>Edit Page</h2>
        <?php
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $name = mysqli_real_escape_string($db->link, $_POST['name']);
            $body = mysqli_real_escape_string($db->link, $_POST['body']);


            if ($name == "" || $body == "") {
                echo "<span style='font-size: 18px; color: red;'>Field Must Not Be Empty!</span>";
            } else {
                $query = "UPDATE tbl_page 
                          SET 
                          name = '$name',
                          body = '$body'
                          WHERE id = '$id'";
                $upadated_row = $db->update($query);
                if ($upadated_row) {
                    echo "<span style='font-size: 18px; color: green;'>Page Updated Successfully.</span>";
                } else {
                    echo "<span style='font-size: 18px; color: red;'>Page Not Updated !</span>";
                }
            }




        }
        ?>

        <div class="block">
            <?php
            $query = "SELECT * FROM tbl_page WHERE id = '$id'";
            $pages = $db->select($query);
            if ($pages){
            while ($result = $pages->fetch_assoc()){

            ?>
            <form action="" method="post">
                <table class="form">

                    <tr>
                        <td>
                            <label>Name</label>
                        </td>
                        <td>
                            <input type="text" name="name" value="<?php echo $result['name'];?>" class="medium" />
                        </td>
                    </tr>



                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Content</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body">
                                <?php echo $result['body'];?>
                            </textarea>
                        </td>
                    </tr>



                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Update" />
                            <span class="delete"><a onclick="return confirm('Are You Sure To Delete!')" href="deletepage.php?delpage=<?php echo $result['id']; ?>">Delete</a></span>
                        </td>
                    </tr>
                </table>
            </form>
            <?php }} ?>
        </div>
    </div>
</div>
<div class="clear">
</div>

<?php include 'inc/footer.php' ?>
